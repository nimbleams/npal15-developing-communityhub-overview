public abstract without sharing class SObjectSelector {
    @testVisible private static final String ID_SET_PARAM = 'idSet';
    @testVisible private static final String ACCESS_ERROR_MESSAGE =
            'Read access to {0} is not available to the user attempting access.';
    @testVisible protected final String RELATION_PARAM = 'relation';
    @testVisible protected final String PERIOD = '.';

    protected final String COMMA = ',';
    protected final String ORDER_BY_FALLBACK = 'CreatedDate';

    /**
     * @description Constructs the Selector.
     */
    public SObjectSelector() { }

    /**
     * @description Implement this method to define the default selection
     *              query for the SObject to be queried.
     * @param idSet An id of sets to retrieve.
     * @return List of SObjects.
     */
    public virtual List<SObject> selectSObjectsById(Set<Id> idSet) {
        NC.ArgumentNullException.throwIfNull(idSet, ID_SET_PARAM);
        assertIsAccessible();
        return Database.query(buildQuerySObjectById());
    }

    /**
     * @description Constructs the default SOQL query for this selector.
     * See selectSObjectsById.
     */
    private String buildQuerySObjectById() {
        return String.format('SELECT {0} FROM {1} WHERE id in :idSet ORDER BY {2}',
            new List<String>{getFieldListString(),getSObjectName(),getOrderBy()});
    }

    /**
     * @description Gets the default fields defined in a selector class to be used as part of a sub-query
     *              in another selector class.
     * @param relation A string representing the name of the object relation field.
     * @return A set of strings representing the names of the default relation fields.
     */
    public virtual Set<String> getRelatedFields(String relation) {
        NC.ArgumentNullException.throwIfBlank(relation, RELATION_PARAM);

        Set<String> relatedFieldSet = new Set<String>();
        for (Schema.SObjectField field : getSObjectFieldList()) {
            String relatedField = relation + PERIOD + field.getDescribe().getName();
            relatedFieldSet.add(relatedField);
        }

        return relatedFieldSet;
    }

    /**
     * @description Gets the default fields defined in a selector class to be used as part of a query which
     *              requires relation fields in another selector class.
     * @param relation A string representing the name of the object relation field.
     * @return A string representing the relation fields.
     */
    public String getRelatedFieldListString(String relation) {
        NC.ArgumentNullException.throwIfBlank(relation, RELATION_PARAM);

        Set<String> relatedFieldSet = getRelatedFields(relation);

        return String.join(new List<String>(relatedFieldSet), COMMA);
    }

    /**
     * @description Throws an exception if the SObject indicated by
     *              getSObjectType is not accessible to the current
     *              user (read access).
     */
    @testVisible
    protected void assertIsAccessible()
    {
        if (!getSObjectType().getDescribe().isAccessible()) {
            throw new NC.UnauthorizedAccessException(
                    String.format(ACCESS_ERROR_MESSAGE, new List<String> { getSObjectName() }));
        }
    }

    /**
     * @description Gets the name of the implemented Selector's object.
     * @return The name of the object as a string.
     */
    @testVisible
    protected String getSObjectName() {
        return getSObjectType().getDescribe().getName();
    }

    /**
     * @description Takes a list of fields and returns a comma-delimited string.
     *              Override this method if extra fields are needed that cannot be
     *              included in the field list (IE fields we cannot reference directly
     *              without creating a dependency).
     *
     *              Note the use of sets here so that we avoid duplicates.
     * @param fieldSets A list of fieldSets to include in result fields.
     * @return A string containing the comma-delimited names of the default fields.
     */
    @testVisible
    protected virtual String getFieldListString(List<Schema.FieldSet> fieldSets) {
        Set<String> flds = getFieldListStringSet();
        flds.addAll(getFieldSetsAsStringSet(fieldSets));

        return String.join(new List<String>(flds), COMMA);
    }

    /**
     * @description Constructs a string representing the default fields to query plus additional fields.
     * @param additionalFields A set of additional fields to query for beyond the base field list.
     * @return A string containing the comma-delimited names of the fields.
     */
    @testVisible
    protected virtual String getFieldListString(Set<String> additionalFields) {
        Set<String> fieldSet = getFieldListStringSet();
        fieldSet.addAll(additionalFields);

        return String.join(new List<String>(fieldSet), COMMA);
    }

    /**
     * @description Constructs a string representing the default fields to query.
     * @return A string containing the comma-delimited names of the default fields.
     */
    public String getFieldListString() {
        List<String> fieldStrings = new List<String>();
        for (Schema.SObjectField f : getSObjectFieldList()) {
             fieldStrings.add(f.getDescribe().getName());
        }
        return String.join(fieldStrings, COMMA);
    }

    private String orderBy;
    /**
     * @description Controls the default ordering of records returned by the base queries.
     *      Defaults to the name field of the object or CreatedDate if there is none.
     * @return Default ordering of records returned by the base queries.
     **/
    public virtual String getOrderBy() {
        if (orderBy == null) {
            orderBy = getNameField() == null ? ORDER_BY_FALLBACK : getNameField().getDescribe().getName();
        }
        return orderBy;
    }

    private Schema.SObjectField nameField;
    /**
    * @description Gets the name field for the sObject Type.
    *       If the sObject does not have a name field, the method returns null.
    * @return The name field of the sObject. Null if there is no name field.
    */
    private Schema.SObjectField getNameField() {
        if (nameField != null) {
            return nameField;
        }
        for (Schema.SObjectField field : getSObjectType().getDescribe().fields.getMap().values()) {
            if (field.getDescribe().isNameField()) {
                nameField = field;
                break;
            }
        }
        return nameField;
    }

    /**
     * @description Takes in a list of fields, returns a list of their names.
     * @return A set of field names as strings.
     */
    protected Set<String> getFieldListStringSet() {
        Set<String> fieldStrings = new Set<String>();
        for (Schema.SObjectField f : getSObjectFieldList()) {
             fieldStrings.add(f.getDescribe().getName());
        }

        return fieldStrings;
    }

    /**
     * @description Retrieves the field sets pertinent to this Selector
     *              and returns a list of all their field names as strings.
     *
     *              Note the internal use of sets so as to avoid
     *              duplicating fields.
     * @param fieldSets A list of fieldSets to stringify as well.
     * @return A list of field names as strings.
     */
    @testVisible
    protected Set<String> getFieldSetsAsStringSet(List<Schema.FieldSet> fieldSets) {
        Set<String> flds = new Set<String>();
        if (fieldSets == null) {
            return flds;
        }

        for (Schema.FieldSet currFieldSet : fieldSets) {
            for (FieldSetMember f : currFieldSet.getFields()) {
                flds.add(f.getFieldPath());
            }
        }

        return flds;
    }

    /**
     * @description Implement this method to inform the base class
     *              of the SObject (custom or standard) to be queried.
     */
    abstract Schema.SObjectType getSObjectType();

    /**
     * @description Implement this method to inform the base class
     *              of the common fields to be queried or listed by
     *              the base class methods.
     */
    abstract List<Schema.SObjectField> getSObjectFieldList();
}