/**
 * @description Controls CustomNotification.Component
 */
public without sharing class CustomNotificationComponentController {
    @testVisible private static final String BLANK_STRING = '';

    private String currentUserFirstName;

    /**
     * @description Gets the first name of the currently logged in user.
     * @return The current user's first name.
     */
    public String getCurrentUserFirstName() {
        if (currentUserFirstName == null) {
            if (NC.CommunityUser.IsLoggedIn) {
                List<Account> accounts = new AccountSelector().selectById(new Set<Id> { NC.CommunityUser.getAccountId() });
                currentUserFirstName = accounts[0].FirstName;
            } else {
                currentUserFirstName = BLANK_STRING;
            }
        }

        return currentUserFirstName;
    }
}