/**
 * @description The home of all SOQL queries for Account records.
 **/
public without sharing class AccountSelector extends SObjectSelector {
    @testVisible private static final String ACCOUNT_IDS_PARAM = 'accountIds';
    @testVisible private static final String ORDER_BY_FORMAT = '{0}, {1}';

    private static final String IS_PERSON_ACCOUNT_FIELD = 'IsPersonAccount';

    /**
     * @description Selects a set of Accounts by their id's.
     * @param accountIds The id's to select by.
     * @return A list of Account objects.
     */
    public List<Account> selectById(Set<Id> accountIds) {
        return selectById(accountIds, new List<Schema.FieldSet>());
    }

    /**
     * @description Selects a set of Accounts by their id's.
     * @param accountIds The id's to select by.
     * @param fieldSets A list of fieldsets to select along with the default fields.
     * @return A list of accounts.
     */
    public List<Account> selectById(Set<Id> accountIds, List<Schema.FieldSet> fieldSets) {
        NC.ArgumentNullException.throwIfNull(accountIds, ACCOUNT_IDS_PARAM);

        assertIsAccessible();
        return Database.query(buildSelectByAccountIdQuery(fieldSets));
    }

    private List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Account.Name,
            Account.FirstName,
            Account.LastName,
            Account.IsPersonAccount,
            Account.PersonEmail,
            Account.nu__PersonContact__c,
            Account.nu__Member__c,
            Account.nu__MemberThru__c,
            Account.nu__PrimaryAffiliation__c,
            Account.nu__RecordTypeName__c
        };
    }

    /**
     * @description Returns the basic, standard field list for account queries plus the
     *              standard IsPersonAccount field and any supplied fieldsets.
     * @param fieldSets A list of any relevant fieldsets.
     * @return A comma-delimited string representing the list of fields to be queried.
     */
    protected override String getFieldListString(List<Schema.FieldSet> fieldSets) {
        Set<String> flds = getFieldListFields(fieldSets);
        return String.join(new List<String>(flds), COMMA);
    }

    private Set<String> getFieldListFields(List<Schema.FieldSet> fieldSets) {
        Set<String> flds = getFieldListStringSet();
        if (fieldSets != null && fieldSets.size() > 0) {
            flds.addAll(getFieldSetsAsStringSet(fieldSets));
        }
        flds.add(IS_PERSON_ACCOUNT_FIELD);

        return flds;
    }

    private String buildSelectByAccountIdQuery(List<Schema.FieldSet> fieldSets) {
        return buildSelectXByIdQuery(getFieldListString(fieldSets));
    }

    private String buildSelectXByIdQuery(String fieldList) {
        return String.format('SELECT {0} FROM {1} WHERE Id IN :accountIds ORDER BY {2}', new List<String> { fieldList, getSObjectName(), getOrderBy() });
    }

    private Schema.SObjectType getSObjectType() {
        return Account.sObjectType;
    }

    /**
    * @description Default ordered by last name, name for selecting accounts.
    * @return LastName, Name.
    */
    public override String getOrderBy() {
        return String.format(ORDER_BY_FORMAT, new List<String> {
                 Account.LastName.getDescribe().getName(),
                 Account.Name.getDescribe().getName() });
    }
}