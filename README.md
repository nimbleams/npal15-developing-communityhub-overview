# Developing within Community Hub - Overview
### Craig Ceremuga | Wednesday, October 28, 2015 | 1:00 PM - 1:55 PM
An introduction to Community Hub from a developer’s perspective. Covering templating, theming, custom components, selecting records, basic customizations.
